
let messageData = []

function addMessage(msg) {
  messageData.unshift(msg)
}
function getMessage() {
  return messageData
}

module.exports = { addMessage, getMessage }