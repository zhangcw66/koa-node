
const Koa = require('koa')
const koaBody = require('koa-body')
const cors = require('koa-cors')
const httpPort = require('./config.js')
const server = new Koa()

let router = require('./router/index.js')

server.use(cors())
server.use((ctx, next) => {
  return next()
})
server.use(koaBody())
server.use(router.routes())

server.listen(httpPort.httpPort, () => {
  console.log('server is running localhost:' + httpPort.httpPort)
})
