# 一个Koa框架搭建的基本node服务

```
npm install
```


### 启动服务

```
npm start
或
node server.js
```

### 热更新启动

```
nodemon server.js
```
