
let getData = require('./getData.js')
let addData = require('./addData.js')

module.exports = [
  getData,
  addData
]