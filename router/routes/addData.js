
// POST 请求 body里取参数

let database = require( '../../database/index.js')

async function handle(ctx, next) {
  let params = ctx.request.body
  if (params) {
    params.dateTime = new Date().toLocaleString()
    ctx.body = { data: params, status: 200, message: '成功' }
    database.addMessage(params)
  } else {
    ctx.body = { data: {}, status: 500, message: '参数错误' }
  }
}
module.exports = {
  path: '/add-message',
  handle
}