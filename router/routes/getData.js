
// GET 请求 query 里取参数

let database = require('../../database/index.js')

async function handle(ctx, next) {
  let query = ctx.request.query
  let messageData = database.getMessage()
  ctx.body = { data: { messageData }, status: 200, message: '成功' }
}

module.exports = {
  path: '/get-message',
  handle
}