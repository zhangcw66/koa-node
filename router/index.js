
const router = new require('koa-router')()

let routes = require('./routes/index.js')
routes.forEach(r => {
  router.all(r.path, r.handle)
})

module.exports = router